import pickle

from utility import *

from qiskit.aqua.algorithms import QAOA
from qiskit.aqua.components.optimizers import COBYLA
from qiskit.providers.aer.backends.qasm_simulator import QasmSimulator

filename = 'starbattle_4x4_qubo.pickle'

with open(filename, 'rb') as f:
    sympy_qubo = pickle.load(f)

ising_model = sympy_qubo_to_ising(sympy_qubo)
ising_model_spins = sympy_vars(ising_model)

print('**** Ising model spins ****')
print(ising_model_spins)

print('**** Ising model ****')
ising_model *= 4.0
print(ising_model)

ising_model_spin_names = [spin.name for spin in ising_model_spins]
print('**** Ising model spin names ****')
print(ising_model_spin_names)

ising_model_spin_names.sort()
spin_order = {Symbol(spin_name): i for i, spin_name in enumerate(ising_model_spin_names)}

print('**** Ising model spin order ****')
print(spin_order)

print('**** Hamiltonian ****')
H = sympy_ising_to_Hamiltonian(ising_model, spin_order)
print(H)

print('**** Running QAOA ****')
opt = COBYLA()
qaoa = QAOA(H, opt, p=1)
backend = QasmSimulator()
response = qaoa.run(backend)

print('**** Response ****')
for key, val in response.items():
    if key not in {'min_vector', 'eigvecs'}:
        print('%20s = ' % key, val)

vec = qaoa.get_optimal_vector()

print('**** Optimal vector ****')
print(vec)
