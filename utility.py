"""Utility functions for converting Sympy QUBOs to Sympy Ising models and Hamiltonians."""

from sympy import Symbol
from qiskit.quantum_info.operators.pauli import Pauli
from qiskit.aqua.operators.legacy.weighted_pauli_operator import WeightedPauliOperator


def sympy_qubo_to_ising(sympy_qubo, verbose=False, qubo_base='x', ising_base='s'):
    """Convert a Sympy QUBO to a Sympy Ising model."""
    qubo_vars = sympy_qubo.expand().as_terms()[1]

    if verbose:
        print('**************** QUBO variables ****************')
        print(qubo_vars)

    qubo_to_spin = dict()

    for qubo_var in qubo_vars:
        ising_spin = Symbol(qubo_var.name.replace(qubo_base, ising_base))
        qubo_to_spin[qubo_var] = (1 - ising_spin)/2

    if verbose:
        print('**************** QUBO to Ising ****************')
        print(qubo_to_spin)

    ising = sympy_qubo.subs(qubo_to_spin)

    if verbose:
        print('**************** Ising ****************')
        print(ising)

    ising_expanded = ising.expand()
    terms = ising_expanded.as_terms()

    if verbose:
        print('**************** Ising spins ****************')
        print(terms[1])

    ising_model = 0

    for term in ising_expanded.args:
        coeff = 1.0
        degree = 0
        vars = []
        for key, val in term.as_powers_dict().items():
            if type(key) == Symbol:
                if val == 1:
                    degree += 1
                    vars.append(key)
                elif val == 2:
                    pass
                else:
                    print('error: unexpected term')
                    return None
            else:
                coeff = float(key)
        if degree == 0:
            ising_model += coeff
        elif degree == 1:
            ising_model += coeff * vars[0]
        elif degree == 2:
            ising_model += coeff * vars[0] * vars[1]

    return ising_model



def build_Pauli(S, limit):
    """Generate a product of Pauli operators."""
    return Pauli(z=[i in S for i in range(limit)], x=[False] * limit)


def sympy_ising_to_Hamiltonian(sympy_ising, spin_order):
    """Convert a Sympy Ising model to a Qiskit WeightedPauliOperator."""
    ising_expanded = sympy_ising.expand()

    term_list = []

    num_spins = len(spin_order)
    
    for term in ising_expanded.args:
        coeff = 1.0
        degree = 0
        vars = []
        for key, val in term.as_powers_dict().items():
            if type(key) == Symbol:
                if val == 1:
                    degree += 1
                    vars.append(key)
                elif val == 2:
                    pass
                else:
                    print('error: unexpected term')
                    return None
            else:
                coeff = float(key)
        if degree == 0:
            term_list.append([coeff, build_Pauli({}, num_spins)])
        elif degree == 1:
            term_list.append([coeff, build_Pauli({spin_order[vars[0]]}, num_spins)])
        elif degree == 2:
            term_list.append([coeff, build_Pauli({spin_order[vars[0]], spin_order[vars[1]]}, num_spins)])

    return WeightedPauliOperator(term_list)


def sympy_vars(sympy_expr):
    """List the variables in a Sympy expression."""
    return sympy_expr.expand().as_terms()[1]
