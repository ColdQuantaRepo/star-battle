from sympy import Symbol
from numpy import zeros


def sympy_to_numpy_qubo(var_order, sympy_qubo):
    size = len(var_order)
    numpy_qubo = zeros([size, size])
    constant_term = 0.0
    for term in sympy_qubo.expand().args:
        coeff = 1.0
        degree = 0
        vars = []
        for key in term.as_powers_dict().keys():
            if type(key) == Symbol:
                degree += 1
                vars.append(key)
            else:
                coeff = float(key)
        if degree == 0:
            constant_term += coeff
        elif degree == 1:
            numpy_qubo[var_order[vars[0]], var_order[vars[0]]] += coeff
        elif degree == 2:
            numpy_qubo[var_order[vars[0]], var_order[vars[1]]] += coeff / 2.0
            numpy_qubo[var_order[vars[1]], var_order[vars[0]]] += coeff / 2.0

    return constant_term, numpy_qubo