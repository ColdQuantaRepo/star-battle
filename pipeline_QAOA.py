import numpy as np
import matplotlib.pyplot as plt

from itertools import product

from qiskit import Aer
from qiskit.quantum_info.operators.pauli import Pauli
from qiskit.aqua.operators.legacy.weighted_pauli_operator import WeightedPauliOperator
from qiskit.aqua.algorithms import QAOA
from qiskit.aqua.components.optimizers import COBYLA

backend1 = Aer.get_backend('statevector_simulator')
backend2 = Aer.get_backend('qasm_simulator')

def qubo_to_ising_model(qubo):
    # Takes a matrix representing a [0,1] qubo, and transforms it into an ising model
    # The ising model will have two components: the strict upper triangular matrix of quadratic terms B and
    # the vector of linear coefficients A. The model will be evaluated by the objective function xTBx + ATx
    n = qubo.shape[0]
    A = np.zeros((n, 1))
    for k in range(n):
        A[k, 0] -= sum([qubo[k, j] for j in range(n) if k != j]) / 4
        A[k, 0] -= sum([qubo[j, k] for j in range(n) if k != j]) / 4
        A[k, 0] -= qubo[k, k] / 2

    B = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            if j > i:
                B[i, j] += qubo[i, j] / 2

    # the final results are multiplied by 4 to ensure the the final model, when converted to a hamiltonian will have
    # integer eigenvalues (assuming that all the entries in the original qubo were integers)
    return 4 * B, 4 * A.reshape((n, 1))


def get_results_ising(B, A, show=False):
    # This function validates that the ising model works as intended, allowing us to be confident that the ising model
    # from qubo_to_ising_model does indeed solve the same problem as the qubo (used in the context of the star battle
    # puzzle)

    # Set show=True if multiple solutions are expected
    # This parameter is good for testing that subqubos are producing satisfactory states

    mat_size = B.shape[0]
    grid_size = int(np.sqrt(mat_size))
    states = list(product([1, -1], repeat=mat_size))
    outputs = {}

    for state in states:
        x = np.array(state).reshape((mat_size, 1))
        obj = x.T @ B @ x + A.T @ x
        outputs[state] = obj[0, 0]

    minimum_value = min(outputs.values())
    solutions = [key for key in outputs if outputs[key] == minimum_value]

    if show:
        for solution in solutions:
            plt.figure(figsize=(2, 2))
            plt.imshow(np.array(solution).reshape((grid_size, grid_size)))
            plt.show()

    return minimum_value, np.array(solutions[0]).reshape((grid_size, grid_size))


def ising_to_WPO(B, A):
    # B is assumed to be a strict upper triangular matrix containing the coefficients for the quadratic terms
    # A is assumed to be the n x 1 vector of linear coefficients
    n = B.shape[0]
    terms = [] # List to hold coefficient-Pauli pairs to form the weighted Pauli Operator
    for i in range(n):
        for j in range(n):
            if j==i:
                # add the diagonal weight with single qubit Pauli-Z operator
                terms.append([A[i,0],Pauli(z=[i==k for k in range(n)], x=[False]*n)])
            elif j>i:
                # add the off-diagnoal (upper triangular) weight with the i-j two qubit Pauli-Z operator
                terms.append([B[i,j],Pauli(z=[i==k or j==k for k in range(n)], x=[False]*n)])
    return WeightedPauliOperator(terms)


def run_qaoa(Hamiltonian, p, backend = backend2):
    optimizer = COBYLA()
    qaoa = QAOA(Hamiltonian, optimizer=optimizer, p=p)
    return qaoa.run(backend=backend)


def generate_QAOA_circ(Hamiltonian, p, backend=backend2):
    optimizer = COBYLA()
    qaoa = QAOA(Hamiltonian, optimizer=optimizer, p=p)
    return qaoa.get_optimal_circuit()


def qubo_to_qaoa(qubo, p):
    B, A = qubo_to_ising_model(qubo)
    Hamiltonian = ising_to_WPO(B, A)
    optimizer = COBYLA()
    return QAOA(Hamiltonian, optimizer=optimizer, p=p)
