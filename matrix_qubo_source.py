import numpy as np
import matplotlib.pyplot as plt
from itertools import product


# This file contains the function handles that were defined and used in the Star Battle Matrix Construction Jupyter
# Notebook. The Notebook is designed to be a visual example, and this file is for the convenience of anyone wanting to
# adjust the code themselves.


def adjacency_qubo(grid_size):
    temp = []
    pairs = [(r,c) for r in range(grid_size) for c in range(grid_size)]
    for element1 in pairs:
        for element2 in pairs:
            r1, c1, r2, c2 = element1[0], element1[1], element2[0], element2[1]
            if r1==r2 and c1==c2:
                temp.append(0)
            elif abs(r1-r2)>=2 or abs(c1-c2)>=2:
                temp.append(0)
            else:
                temp.append(1)
    return np.array([temp]).reshape((grid_size**2, grid_size**2))


def region_qubo(puzzle):
    mat_size = len(puzzle)
    region_dict = {k:puzzle[k] for k in range(mat_size)}
    region_qubo = np.zeros((mat_size, mat_size))
    for i in range(mat_size):
        for j in range(mat_size):
            if i!=j and region_dict[i]==region_dict[j]:
                region_qubo[i,j]+=1
    return region_qubo - np.identity(mat_size)


def row_as_region(grid_size):
    regions = []
    for i in range(grid_size**2):
        regions.append(i//grid_size)
    return regions


def col_as_region(grid_size):
    regions = []
    for i in range(grid_size**2):
        regions.append(i%grid_size)
    return regions


def make_qubo(puzzle):
    grid_size = int(np.sqrt(len(puzzle)))
    qubo = np.zeros((grid_size**2,grid_size**2))
    qubo += region_qubo(row_as_region(grid_size))
    qubo += region_qubo(col_as_region(grid_size))
    qubo += region_qubo(puzzle)
    qubo += adjacency_qubo(grid_size)
    return qubo


def get_results(Q, show=False):
    # Set show=True if multiple solutions are expected
    # This parameter is good for testing that subqubos are producing satisfactory states
    mat_size = Q.shape[0]
    grid_size = int(np.sqrt(mat_size))
    states = list(product([0,1], repeat = mat_size))
    outputs = {}
    for state in states:
        x = np.array(state).reshape((mat_size,1))
        obj = x.T @ Q @ x
        outputs[state] = obj[0,0]
    minimum_value = min(outputs.values())
    solutions = [key for key in outputs if outputs[key]==minimum_value]
    if show:
        for solution in solutions:
            plt.figure(figsize=(2,2))
            plt.imshow(np.array(solution).reshape((grid_size,grid_size)))
            plt.show()
    return minimum_value, np.array(solutions[0]).reshape((grid_size,grid_size))

# Start of Addendum Section

def line_to_grid(k, grid_size):
    return k//grid_size, k%grid_size


def grid_to_line(i,j,grid_size):
    return grid_size*i +j


def row_qubo(grid_size):
    qubo = np.zeros((grid_size**2, grid_size**2))
    single_row = np.ones((grid_size, grid_size)) - 2*np.identity(grid_size)
    for row_block in range(grid_size):
        for col_block in range(grid_size):
            if row_block == col_block:
                row_start = row_block*grid_size
                row_stop = (row_block+1)*grid_size
                col_start = col_block*grid_size
                col_stop = (col_block+1)*grid_size
                qubo[row_start:row_stop, col_start:col_stop] = single_row
    return qubo


def col_qubo(grid_size):
    row_matrix = row_qubo(grid_size)
    col_qubo = np.zeros((grid_size**2, grid_size**2))
    for k1 in range(grid_size**2):
        for k2 in range(grid_size**2):
            i1,j1 = line_to_grid(k1, grid_size)
            i2,j2 = line_to_grid(k2, grid_size)
            k1_trans = grid_to_line(j1,i1, grid_size)
            k2_trans = grid_to_line(j2,i2, grid_size)
            col_qubo[k1_trans, k2_trans] += row_matrix[k1, k2]
    return col_qubo
