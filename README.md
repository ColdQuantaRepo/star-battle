# Overview
This project goes through the process of encountering a problem in the world, formatting it as a QUBO, and translating it into a format that is ideal for running on a quantum computer. The problem in context is the Star Battle puzzle outlined in the video segments called 3 minutes of quantum computing. Included are two main "routes" for constructing a QUBO. One is by building the algebraic expression through the sympy package in python. The other is by buidling the matrices directly using the numpy package in python.

The following packages are used in the project: sympy, numpy, matplotlib, jupyter notebooks, and qiskit. If the environment you are using does not have these required packages, they can be installed by a simple pip install (see the specific installation guidelines on their websites for more info). Additionally, a package called qatalyst gets rapid solutions to large QUBOs is in the code, but the average user will not be able to run this part of the code without their own access token. Consider checking out https://www.quantumcomputinginc.com/ for more details on Qatalyst.

The code and jupyter notebooks are meant to accompany the video segments, so it is recommended that those be watched for context surrounding the subject matter. The general recommended path is to start with either the [sympy](Star Battle Matrix Construction.ipynb) or the [matrix](Star Battle Matrix Construction.ipynb) construction jupyter notebook depending your preference between algebraic expressions or matrices. Read through the notebook, understand how to convert the Star Battle puzzle into a QUBO, and see how an answer might look. From there if you are curious about the code or want to adjust it for yourself, the source code is available as a py file.

Next we recommend looking at the [QAOA](Using QAOA on the Star Battle Qubo.ipynb) jupyter notebook for a more intimate look at what quantum computation looks like on a gate model machine using the more modest limitations of the current state of quantum computers. There is much to unpack in dealing with qiskit and QAOA so the noebook is merely a general overview useful for someone getting started in the quantum computing space or someone with a little experience using packages like qiskit who wants to add a useful algorithm to their toolbox.

# Workflow Diagram for Constructing QUBOs
The following image depicts two ways to go through QUBO construction that are outlined in the jupyter notebooks. Both ultimately lead to an exact result from Qatalyst so feel free to go any way that you like, or both!
![Workflow A](Workflow A.PNG)

# Workflow Diagram for Running QAOA
The image below shows the intermediate steps taken to turn a scaled down version of the Star Battle QUBO into something that can be processed by qiskit. Each step is a part in the accompanying jupyter notebook.
![Workflow B](Workflow B.PNG)
