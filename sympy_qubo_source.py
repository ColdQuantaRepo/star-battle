from math import sqrt
from sympy import Symbol
from itertools import product
import matplotlib.pyplot as plt
import numpy as np
import sympy_to_numpy_qubo

# This file contains the function handles that were defined in the Star Battle Sympy Construction Jupyter Notebook
# The Notebook is designed to be a visual example, and this file is for the convenience of anyone wanting to adjust the
# code for themselves


def qubo_for_row_constraint(grid_size, var_dict, row):
    row_vars = [var_dict[(row, column)] for column in range(grid_size)]
    return (sum(row_vars) - 1)**2


def qubo_for_column_constraint(grid_size, var_dict, column):
    column_vars = [var_dict[(row, column)] for row in range(grid_size)]
    return (sum(column_vars) - 1)**2


def qubo_for_region_constraint(grid_size, var_dict, region, puzzle):
    region_indices = [i for i in range(len(puzzle)) if puzzle[i] == region]
    region_vars = [var_dict[(ind//grid_size, ind%grid_size)] for ind in region_indices]
    return (sum(region_vars) - 1)**2


def varname(r, c):
    return 'x_' + str(r) + '_' + str(c)


def make_qubo(puzzle):

    grid_size = int(sqrt(len(puzzle)))

    var_dict = {(r, c): Symbol(varname(r, c))
                for r in range(grid_size) for c in range(grid_size)}

    qubo = 0

    for row in range(grid_size):
        qubo += qubo_for_row_constraint(grid_size, var_dict, row)

    for column in range(grid_size):
        qubo += qubo_for_column_constraint(grid_size, var_dict, column)

    for region in set(puzzle):
        qubo += qubo_for_region_constraint(grid_size, var_dict, region, puzzle)

    for row, col in var_dict.keys():
        if row != grid_size-1:
            qubo += var_dict[(row, col)] * var_dict[(row+1, col)]

        if col != grid_size-1:
            qubo += var_dict[(row, col)] * var_dict[(row, col+1)]

        if row != grid_size-1 and col != grid_size-1:
            qubo += var_dict[(row, col)] * var_dict[(row+1, col+1)]

        if row != 0 and col != grid_size-1:
            qubo += var_dict[(row, col)] * var_dict[(row-1, col+1)]

    var_order = {var:i for i, var in enumerate(var_dict.values())}

    return sympy_to_numpy_qubo.sympy_to_numpy_qubo(var_order, qubo)


def get_results(Q, show=False):
    # Set show=True if multiple solutions are expected
    # This parameter is good for testing that subqubos are producing satisfactory states
    mat_size = Q.shape[0]
    grid_size = int(np.sqrt(mat_size))
    states = list(product([0,1], repeat = mat_size))
    outputs = {}
    for state in states:
        x = np.array(state).reshape((mat_size,1))
        obj = x.T @ Q @ x
        outputs[state] = obj[0,0]
    minimum_value = min(outputs.values())
    solutions = [key for key in outputs if outputs[key]==minimum_value]
    if show:
        for solution in solutions:
            plt.figure(figsize=(2,2))
            plt.imshow(np.array(solution).reshape((grid_size,grid_size)))
            plt.show()
    return minimum_value, np.array(solutions[0]).reshape((grid_size,grid_size))